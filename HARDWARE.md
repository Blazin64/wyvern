## Hardware Guide (Work in progress)
Note: This is not for beginners.  These instructions are more of a summary rather than baby steps. Don't do this unless you know what you're doing.

### Printed Parts

Before you begin, you need to print the following parts:
1. All STL part files in the [STL folder](STL).
2. Download the official Prusa MK3S parts from their site. Print all of the official Prusa MK3S E, X, and Z-axis parts. (Don't print the 4 official Prusa top and bottom Z-axis mounts. They are replaced by the parts provided here on GitLab.)
3. Download and print this [lcd mount by BoothyBoothy](https://www.thingiverse.com/thing:4635067)  on Thingiverse.

### Disassembly
1. Unplug ALL cables, remove the LCD display, and remove the electronics box.
2. Remove the Z-axis motor and leadscrew. Be sure not to let the X-axis gantry fall!
3. Remove the X-axis gantry by unbolting the top 2020 extrusion and sliding the gantry off. When you finish that, go ahead and put the top 2020 extrusion back on.

### Y-axis
4. Install a linear rail Y-axis mod of your choice. Just follow the instructions they gave you for that. Retail linear rail kits make this a very simple process. Personally, I used the one from [Gulf Coast Robotics](https://gulfcoast-robotics.com/products/mgn12-linear-rails-kit-ender-3). (No affiliation) It happened to be the easiest option at the time I purchased most of the parts.

### Check
5. Verify that the only things on your Ender 3 frame are the Y-axis hardware and the power supply.

### X and Z-axis
6. Follow the [Prusa MK3S X-axis instructions](https://help.prusa3d.com/guide/3-x-axis-assembly_167854).
   * Use the 350mm rods in place of Prusa's 330mm rods.
7. Follow the [Prusa MK3S Z-axis instructions](https://help.prusa3d.com/guide/4-z-axis-assembly_168629).
   * Substitute my 4 Z brackets for the official Prusa ones.
   * Use the 384mm rods in place of Prusa's 320mm rods.
   * Use your own leadscrews, leadscrew nuts, Z motors, and couplers in place of Prusa's custom Z motors.
8. Follow the [Prusa MK3S E-axis instructions](https://help.prusa3d.com/guide/5-e-axis-assembly_169235). No substitutions needed!

### Electronics

Note: This section is incomplete.

9. Install the new LCD mount and transfer the stock Ender 3 LCD to it.
10. Install the new electronics case and connect all cables appropriately. Use the Prusa to SKR adapter cables for the extruder hardware if needed. Please try to keep your cables tidy!

### Firmware

Note: This section is incomplete.

You will need to install Klipper on your Raspberry Pi 4 to continue. There is a tutorial in the [official Klipper documentation](https://www.klipper3d.org/Installation.html).

My Klipper configuration file can be found in the [Klipper folder](Klipper).
