# Wyvern

This is a Creality Ender 3 Pro modified to use a Prusa i3 MK3S X and Z axis motion system.

![photo of printer](photo.jpg)

Note: The design (the version published here) does not include the MMU2S for multi-material printing. I recently switched to the [Klipper firmware](https://www.klipper3d.org/), which does not support the MMU2S. In the future, I will replace the MMU2S with the [ERCF](https://github.com/EtteGit/EnragedRabbitProject).

## Specs

* Cost: $531.76 USD\*
* Bill of Materials: [BOM.pdf](BOM.pdf) or [BOM.ods](BOM.ods)
* Build Volume: 233 x 233 x 255mm
* Firmware: Klipper
* Electronics: Raspberry Pi 4 + BTT SKR 1.4 Turbo
* Stepper Drivers: TMC2209
* Endstops: Sensorless
* Bed Probe: BL-Touch
* X-Axis: 8mm smooth rods, belt driven
* Y-Axis: MGN12 linear rails, belt driven
* Z-Axis: 8mm smooth rods, leadscrew driven, dual steppers

\* The pricing is from 2021. The cost of purchasing an Ender 3 Pro is not included.

## [Hardware Guide (Work in progress)](HARDWARE.md)

## FAQ

* Why did you make this?
  * I enjoy tinkering. Also, I am SICK of poorly manufactured V-wheels and out-of-spec V-slots!
* How's the print quality?
  * In my case, it is significantly better than the stock Ender 3 Pro. It is seems to be equivalent to an i3 MK3. Your mileage may vary, of course.

## Source

All CAD designs can be found in the [CAD](CAD) folder. (One of them is currently not published, but will be soon.)
